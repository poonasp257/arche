// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArcheGameModeBase.generated.h"

UCLASS()
class ARCHE_API AArcheGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AArcheGameModeBase();
};
