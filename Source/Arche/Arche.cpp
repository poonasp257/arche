// Copyright Epic Games, Inc. All Rights Reserved.

#include "Arche.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Arche, "Arche" );

DEFINE_LOG_CATEGORY(ARCLog);