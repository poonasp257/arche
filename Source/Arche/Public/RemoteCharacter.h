#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RemoteCharacter.generated.h"

UCLASS()
class ARCHE_API ARemoteCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ARemoteCharacter();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};