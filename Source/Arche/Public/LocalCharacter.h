#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "LocalCharacter.generated.h"

UCLASS()
class ARCHE_API ALocalCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ALocalCharacter();

	virtual void SetupPlayerInputComponent( class UInputComponent* PlayerInputComponent ) override;

private:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual void Jump() override;
	void Attack();

	void MoveForward(float Val);
	void MoveRight(float Val);
	void ZoomInOut(float Val);

private:
	UPROPERTY(VisibleAnywhere)
	class USpringArmComponent* SpringArmComponent = nullptr;

	UPROPERTY(VisibleAnywhere)
	class UCameraComponent* FollowCamComponent = nullptr;

	UPROPERTY(VisibleAnywhere)
	float ZoomFactor = 0.0f;

	UPROPERTY(VisibleAnywhere)
	float JumpDelay = 1.0f;

	UPROPERTY(VisibleAnywhere)
	float LastJumpedTime = 0.0f;
	
	UPROPERTY(VisibleAnywhere)
	float AttackDelay = 1.0f;

	UPROPERTY(VisibleAnywhere)
	float LastAttackedTime = 0.0f;

public:
	UPROPERTY(EditAnywhere)
	class UAnimMontage* AttackAnimMontage = nullptr;

	UPROPERTY(EditAnywhere)
	int32 MinZoomDistance = 75;

	UPROPERTY(EditAnywhere)
	int32 MaxZoomDistance = 900;

	UPROPERTY(EditAnywhere)
	int32 ZoomSpeed = 1500;
};