#include "RemoteCharacter.h"

ARemoteCharacter::ARemoteCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ARemoteCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ARemoteCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARemoteCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}