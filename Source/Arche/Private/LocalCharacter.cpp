#include "LocalCharacter.h"
#include "Arche/Arche.h"
#include <Engine/Classes/Camera/CameraComponent.h>
#include <Engine/Classes/Components/CapsuleComponent.h>
#include <Engine/Classes/GameFramework/SpringArmComponent.h>
#include <Engine/Classes/GameFramework/CharacterMovementComponent.h>
#include <Engine/Classes/Kismet/GameplayStatics.h>

ALocalCharacter::ALocalCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Controller Settings...
	bUseControllerRotationYaw = false;
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;

	if ( auto Movement = GetCharacterMovement() )
	{
		Movement->bOrientRotationToMovement = true;
		Movement->bUseControllerDesiredRotation = false;
		Movement->JumpZVelocity = 300.f;
		Movement->AirControl = 0.1f;
	}

	// Camera Settings...
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>( TEXT( "SpringArm" ) );
	SpringArmComponent->SetupAttachment( GetCapsuleComponent() );
	SpringArmComponent->bUsePawnControlRotation = true;

	FollowCamComponent = CreateDefaultSubobject<UCameraComponent>( TEXT( "FollowCamera" ) );
	FollowCamComponent->SetupAttachment( SpringArmComponent, USpringArmComponent::SocketName );
	FollowCamComponent->bUsePawnControlRotation = false;

	if ( auto controller = GetController() )
	{
		controller->Possess( this );
	}
}

void ALocalCharacter::SetupPlayerInputComponent( UInputComponent* PlayerInputComponent )
{
	Super::SetupPlayerInputComponent( PlayerInputComponent );

	InputComponent->BindAxis( "MoveForward", this, &ALocalCharacter::MoveForward );
	InputComponent->BindAxis( "MoveRight", this, &ALocalCharacter::MoveRight );
	InputComponent->BindAxis( "LookUp", this, &ALocalCharacter::AddControllerPitchInput );
	InputComponent->BindAxis( "Turn", this, &ALocalCharacter::AddControllerYawInput );
	InputComponent->BindAxis( "Zoom", this, &ALocalCharacter::ZoomInOut );

	InputComponent->BindAction( "Jump", IE_Pressed, this, &ALocalCharacter::Jump );
	InputComponent->BindAction( "Attack", IE_Pressed, this, &ALocalCharacter::Attack );
}

void ALocalCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ALocalCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if ( SpringArmComponent )
	{
		float ArmLegnth = SpringArmComponent->TargetArmLength - ( ZoomFactor * ZoomSpeed * DeltaTime );
		SpringArmComponent->TargetArmLength = FMath::Clamp( ArmLegnth, MinZoomDistance, MaxZoomDistance );
	}
}

void ALocalCharacter::Jump()
{
	float CurrentTime = UGameplayStatics::GetRealTimeSeconds( GetWorld() );
	if ( CurrentTime < LastJumpedTime + JumpDelay ) return;

	Super::Jump();
	LastJumpedTime = CurrentTime;
}

void ALocalCharacter::Attack()
{
	float CurrentTime = UGameplayStatics::GetRealTimeSeconds( GetWorld() );
	if ( CurrentTime < LastAttackedTime + AttackDelay ) return;

	if ( auto MeshComponent = GetMesh() )
	{
		if ( UAnimInstance* AnimInstance = MeshComponent->GetAnimInstance() )
		{
			AnimInstance->Montage_Play( AttackAnimMontage );
		}
	}

	LastAttackedTime = CurrentTime;
}

void ALocalCharacter::MoveForward( float Value )
{
	if ( !Controller ) return;

	// find out which way is forward
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation( 0.0, Rotation.Yaw, 0.0 );

	// get forward vector
	const FVector Direction = FRotationMatrix( YawRotation ).GetUnitAxis( EAxis::X );
	AddMovementInput( Direction, Value );
}

void ALocalCharacter::MoveRight( float Value )
{
	if ( !Controller ) return;

	// find out which way is right
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation( 0.0, Rotation.Yaw, 0.0 );

	// get right vector 
	const FVector Direction = FRotationMatrix( YawRotation ).GetUnitAxis( EAxis::Y );
	// add movement in that direction
	AddMovementInput( Direction, Value );
}

void ALocalCharacter::ZoomInOut( float Value )
{
	ZoomFactor = Value;
}